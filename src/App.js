import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Row,
  Col,
  Select,
  Form,
  Upload,
  Button,
  message,
  Input,
  Checkbox
} from "antd";
import {
  UploadOutlined,
  LoadingOutlined,
  FileAddOutlined
} from "@ant-design/icons";
import "antd/dist/antd.css";
import "./App.css";
import data from "./utils/mockData";

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
};

const options = [
  { label: "DFR", value: "DFR" },
  { label: "APR", value: "APR" },
  { label: "SMR", value: "SMR" }
];


const normFile = e => {
  console.log('Upload event:', e);

  if (Array.isArray(e)) {
    return e;
  }

  return e && e.fileList;
};
function App() {
  const [selectedFile, setSelectedFile] = useState(null);
  const [loadedProgress, setLoadedProgress] = useState(0);
  const [types, setTypes] = useState([]);
  const [brands, setBrands] = useState([]);
  const [models, setModels] = useState([]);
  const [engines, setEngines] = useState([]);
  const [ecus, setEcus] = useState([]);
  const [openServiceInput, setOpenServiceInput] = useState(true);
  const [loadingFile, setLoadingFile] = useState(false);
  const [file, setFile] = useState(null);

  useEffect(() => {
    getTypes();
  }, []);

  const uploadProps = {
    multiple: false,
    name: "file"
  };

  function handleChangeType(value) {
    console.log(`selected type ${value}`);
    getBrands(value);
  }

  function handleChangeBrand(value) {
    console.log(`selected brand ${value}`);
    getModels(value);
  }

  function handleChangeModel(value) {
    console.log(`selected model ${value}`);
    getEngines(value);
  }

  function handleChangeEngine(value) {
    console.log(`selected engine ${value}`);
    getEcus(value);
  }

  function handleChangeEcu(value) {
    console.log(`selected ecu ${value}`);
  }

  function getTypes() {
    let types = data.Types;
    types = types && types.map(t => ({ value: t.vehicleTypeID, name: t.name }));
    setTypes(types);
  }

  function getBrands(id) {
    let brands = data.Brands[id];
    brands = brands && brands.map(t => ({ value: t.brandID, name: t.name }));
    setBrands(brands);
  }

  function getModels(id) {
    let models = data.Models;
    models = models && models.map(t => ({ value: t.modelID, name: t.name }));
    setModels(models);
  }
  function getEngines(id) {
    let engines = data.Engines;
    engines =
      engines && engines.map(t => ({ value: t.engineID, name: t.name }));
    setEngines(engines);
  }
  function getEcus(id) {
    let ecus = data.Ecus;
    ecus = ecus && ecus.map(t => ({ value: t.ecuID, name: t.name }));
    setEcus(ecus);
  }

  function onChangeHandler(event) {
    setSelectedFile(event.target.files[0]);
    setLoadedProgress(0);
  }

  

  function customRequest(obj) {
    console.log("obj is", obj);
    setLoadingFile(true);

    obj.onSuccess(obj.file);
  }

  function onSuccessUploadFile(obj) {
    console.log("onSuccessUploadFile:", obj);
    setFile(obj);
    setLoadingFile(false);
  }

  function onChangeUpload(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  }

  function onChangeService(checkedValues) {
    console.log("checkedValues:", checkedValues);
  }

  function handleSubmit(values) {
    console.log('handleSubmit',values)
    try {
      let remove_services = values.remove_services.split(',').filter(o=>o).join('|')
      const data = new FormData()
      data.append("file",file)
      data.append('remove_services',remove_services)
      data.append('service_options',values.service_options)
      axios.post("http://localhost:3001/v1/util/process-automate-file", data, {
        onUploadProgress: ProgressEvent => {
          setLoadedProgress((ProgressEvent.loaded / ProgressEvent.total) * 100);
        }
      });
    }catch(err){
      console.log("error is", err);
    }
   
  }

 
  return (
    <div className="container">
      <Row>
        <Col className="gutter-row" span={6}>
          <Form {...formItemLayout} >
            <Form.Item
              name="type"
              label="Type"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Select onChange={handleChangeType} allowClear>
                {types &&
                  types.map(t => (
                    <Option key={t.value} value={t.value}>
                      {t.name}
                    </Option>
                  ))}
              </Select>
            </Form.Item>

            <Form.Item
              name="brand"
              label="Brand"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Select onChange={handleChangeBrand} allowClear>
                {brands &&
                  brands.map(t => (
                    <Option key={t.value} value={t.value}>
                      {t.name}
                    </Option>
                  ))}
              </Select>
            </Form.Item>

            <Form.Item
              name="model"
              label="Model"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Select onChange={handleChangeModel} allowClear>
                {models &&
                  models.map(t => (
                    <Option key={t.value} value={t.value}>
                      {t.name}
                    </Option>
                  ))}
              </Select>
            </Form.Item>

            <Form.Item
              name="engine"
              label="Engine"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Select onChange={handleChangeEngine} allowClear>
                {engines &&
                  engines.map(t => (
                    <Option key={t.value} value={t.value}>
                      {t.name}
                    </Option>
                  ))}
              </Select>
            </Form.Item>

            <Form.Item
              name="ecus"
              label="Ecu"
              rules={[
                {
                  required: true
                }
              ]}
            >
              <Select onChange={handleChangeEcu} allowClear>
                {ecus &&
                  ecus.map(t => (
                    <Option key={t.value} value={t.value}>
                      {t.name}
                    </Option>
                  ))}
              </Select>
            </Form.Item>
          </Form>
        </Col>

        <Col className="gutter-row" span={18}>
          <Form {...formItemLayout} onFinish={handleSubmit}>
            <Form.Item
              label="Upload file"
              valuePropName="fileList"
              getValueFromEvent={normFile}

             
            >
              <Upload
                {...uploadProps}
                customRequest={customRequest}
                onSuccess={onSuccessUploadFile}
                showUploadList={false}
              >
                {!file && (
                  <p>
                    {loadingFile ? (
                      <LoadingOutlined style={{ fontSize: "20px" }} />
                    ) : (
                      <FileAddOutlined style={{ fontSize: "30px" }} />
                    )}
                  </p>
                )}
              </Upload>
              {file && (
                <div style={{ display: "block" }}>
                  <h4>{file.name}</h4>
                </div>
              )}
            </Form.Item>

            {openServiceInput && (
              <>
                <Form.Item
                  name="remove_services"
                  label="What You want to remove?"
                  rules={[
                    {
                      required: true
                    }
                  ]}
                >
                  <Input placeholder="Enter each service with comma seperate..." />
                </Form.Item>

                <Form.Item name="service_options"
                  label="What you want to fix?"
                >
                  <Checkbox.Group
                    options={options}
                    onChange={onChangeService}
                  />
                </Form.Item>
              </>
            )}

            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
    // <div className="App">
    //   <input type="file" name="file" onChange={onChangeHandler} />
    //   progress is:{loadedProgress}
    //   <button
    //     type="button"
    //     class="btn btn-success btn-block"
    //     onClick={onClickHandler}
    //   >
    //     Upload
    //   </button>

    //   <DatePicker/>
    // </div>
  );
}

export default App;
